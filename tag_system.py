from turing import Halt


class TagSystem(object):
    def __init__(self, deletion, productions, halt=Halt):
        self.deletion = deletion
        self.productions = productions
        self.halt = halt

    def is_halting(self, string):
        return len(string) < self.deletion or string[0] == self.halt

    def production(self, symbol):
        return self.productions.get(symbol, self.halt)

    @property
    def alphabet(self):
        return sum(map(set, self.productions.values), set()) - {self.halt}

    def tag(self, string):
        if self.is_halting(string):
            return string
        return self.unchecked_tag(string)

    def unchecked_tag(self, string):
        return string[self.deletion:] + self.production(string[0])


class TagSystemIterator(object):
    def __init__(self, tag_system, input):
        self.tag_system = tag_system
        self.input = input

    def next(self):
        if self.input is None:
            raise StopIteration()
        out = self.input
        if self.tag_system.is_halting(self.input):
            self.input = None
        else:
            self.input = self.tag_system.unchecked_tag(self.input)
        return out

    def __next__(self):
        return self.next()

    def __iter__(self):
        return self
