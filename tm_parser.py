from io import BytesIO
import string

import turing


class TMDescriptionParser(object):
    """
    Parser for an extremely minimalistic Turing Machine definition language

    Syntax:

    MACHINE     := ALPHABET "," [TAPE ","] STATES
    ALPHABET    := Any character, "," is escaped as "\," and "\" as "\\"
    TAPE        := (Possybly empty) sequence of ALPHABET characters
    STATES      := STATE STATES | e
    STATE       := ID "," TRANSITIONS
    TRANSITIONS := TRANSITION TRANSITIONS | e
    TRANSITION  := OUTPUT DIRECTION DESTINATION ","
    ID          := Any non-empty sequence of alphanumerics or "-" or "_"
    OUTPUT      := Single character from the ALPHABET
    DIRECTION   := "<" (left) | "-" (don't move) | ">" (right) | "^" (up) | "v" (down)
    DESTINATION := ID | e (empty DESTINATION means Halt)

    NOTE: The first character in the ALPHABET should be considered the blank
          value for the tape.
    NOTE: The transitions for each state deduce the input from the ALPHABET
          definition (ie: the 3rd TRANSITION for a state defines the transition
          from that state with the 3rd character of the ALPHABET on the tape.
          This means every state must define exactly as many transitions as
          there are characters in the ALPHABET.
    NOTE: TAPE can be included only if the parser is configured to support it

    EXAMPLE: The binary incrementer machine can be represented like this:
             " 01,a, <b,0>a,1>a,b,1>,1>,0<b,"
    """

    class ParserError(Exception):
        pass

    movement = {
        "<": turing.Instruction.LEFT,
        ">": turing.Instruction.RIGHT,
        "^": turing.Instruction.UP,
        "v": turing.Instruction.DOWN,
        "-": turing.Instruction.NOOP,

        "o": turing.MultiLinearTape.start_output,
        "O": turing.MultiLinearTape.stop_output,
        "i": turing.MultiLinearTape.start_overwrite_input,
        "I": turing.MultiLinearTape.stop_overwrite_input,
        "n": turing.MultiLinearTape.next_output,
        "p": turing.MultiLinearTape.previous_ouput,
    }

    @classmethod
    def machine_from_string(cls, string, parse_tape=False, tape_type=turing.Tape):
        return cls(BytesIO(string), parse_tape, tape_type).parse()

    @classmethod
    def machine_from_file(cls, filename, parse_tape=False, tape_type=turing.Tape):
        with open(filename) as file_obj:
            return cls(file_obj, parse_tape, tape_type).parse()

    def __init__(self, file_obj, parse_tape=False, tape_type=turing.Tape):
        self.input = file_obj
        self.parse_tape = parse_tape
        self.tape_type = tape_type

    def parse(self):
        machine = self._parse_machine()
        if self._getchar(None):
            raise self.ParserError("Extra characters after machine definition")
        return machine

    def _parse_machine(self):
        alphabet = self._parse_alphabet()

        if self.parse_tape:
            tape = self._parse_tape(alphabet)

        machine = self._parse_states(alphabet)
        machine.blank = alphabet[0]

        if self.parse_tape:
            return machine, tape
        else:
            return machine

    def _getchar(self, expected):
        char = self.input.read(1)
        if not char and expected:
            raise self.ParserError("End of stream, expected %s" % expected)
        return char

    def _read_until(self, boundary, required=True):
        result = ""
        while True:
            char = self._getchar(boundary if required else None)
            if char in boundary or (not required and not char):
                return result, char
            result += char

    def _get_alphabetic_string(self):
        result = ""

        while True:
            slice, delim = self._read_until(",\\")
            result += slice
            if delim == ",":
                break
            else:
                result += self._getchar("\\ or ,")

        return result

    def _parse_alphabet(self):
        alphabet = list(self._get_alphabetic_string())

        if not alphabet:
            raise self.ParserError("Empty alphabet")

        if len(alphabet) != len(set(alphabet)):
            raise self.ParserError("Duplicate characters in the alphabet definition")

        return alphabet

    def _parse_tape(self, alphabet):
        tape_data = self._get_alphabetic_string()
        for ch in tape_data:
            if ch not in alphabet:
                raise self.ParserError("Non-alphabet character on the tape: %s" % ch)
        return self.tape_type(data=tape_data, blank=alphabet[0])

    def _parse_state_name(self):
        name, delim = self._read_until(",", False)
        return name

    def _parse_states(self, alphabet):
        states = {}
        first_state = None
        while True:
            state_name = self._parse_state_name()
            if not state_name:
                break

            if not first_state:
                first_state = state_name

            for input_char in alphabet:
                output_char = self._getchar("a state transition output character")
                if output_char == ",":
                    continue
                if output_char == '\\':
                    output_char = self._getchar("\\ or ,")
                movechar = self._getchar("a movement operation")
                movement = self.movement.get(movechar, None)
                if not movement:
                    raise self.ParserError("Invalid movement operation: %s" % movechar)

                dest_state, delim = self._read_until(",")
                if not dest_state:
                    dest_state = turing.Halt
                states[(state_name, input_char)] = turing.Instruction(output_char, movement, dest_state)

        if not states:
            raise self.ParserError("Empty machine")

        return turing.Machine(states, first_state)


class TMDescriptionWriter(object):
    WriterError = TMDescriptionParser.ParserError

    movement = dict(map(reversed, TMDescriptionParser.movement.iteritems()))

    def __init__(self, replace={None: " "}, minimize_names=False):
        self.replace = replace
        self.minimize_names = minimize_names
        if minimize_names:
            self.names = {}
            self.name_alphabet = string.ascii_letters + string.digits + "-_"
            self.name = [0]

    def write(self, obj):
        if isinstance(obj, turing.MachineRunner):
            return self.write_runner(obj)
        elif isinstance(obj, turing.Machine):
            return self.write_machine(obj)
        elif isinstance(obj, turing.Tape):
            return self.write_tape(obj)

    def write_runner(self, runner):
        alphabet, all_instructions = self._prepare_machine(runner.machine)
        output = self._write_alphabet(alphabet)
        output += self.write_tape(runner.tape) + ","
        output += self._write_states(all_instructions, runner.machine.initial_state)
        return output

    def write_machine(self, machine):
        alphabet, all_instructions = self._prepare_machine(machine)

        output = self._write_alphabet(alphabet)
        output += self._write_states(all_instructions, machine.initial_state)

        return output

    def _write_alphabet(self, alphabet):
        return "".join(map(self._write_alphabetic_char, alphabet)) + ","

    def _write_states(self, instructions, initial_state):
        output = self._write_state(initial_state, instructions[initial_state])
        output += "".join(
            map(
                lambda args: self._write_state(*args),
                filter(
                    lambda args: args[0] != initial_state,
                    instructions.iteritems()
                )
            )
        )

        return output

    def _prepare_machine(self, machine):
        alphabet = set()
        states = set()

        for state, instruction in machine.states.iteritems():
            state_name, input = state
            alphabet.add(input)
            states.add(state_name)
            alphabet.add(instruction.write)
            if instruction.next is not turing.Halt:
                states.add(instruction.next)

        alphabet = sorted(alphabet)

        all_instructions = {}

        for state in states:
            instructions = []
            all_instructions[state] = instructions
            for input in alphabet:
                instructions.append(machine.states.get((state, input)))
        return alphabet, all_instructions


    def _write_alphabetic_char(self, char):
        output = ""
        char = self.replace.get(char, char)
        if char == "," or char == "\\":
            output += "\\"
        output += str(char)
        return output

    def _state_name(self, state_name):
        if not self.minimize_names:
            return state_name

        name = self.names.get(state_name, None)
        if name is not None:
            return name

        name = "".join(self.name_alphabet[i] for i in self.name)
        for i in xrange(1, len(name)+1):
            digit = ( self.name[-i] + 1 ) % len(self.name_alphabet)
            self.name[-i] = digit
            if digit != 0:
                break
        else:
            self.name.insert(0, 1)

        self.names[state_name] = name
        return name


    def _write_state(self, state_name, instructions):
        return self._state_name(state_name) + "," + ",".join(
            map(self._write_instruction, instructions)
        ) + ","


    def _write_instruction(self, instruction):
        if not instruction:
            return ""
        return (
            self._write_alphabetic_char(instruction.write) +
            self.movement.get(instruction.move, '?') +
            ("" if instruction.next is turing.Halt else self._state_name(instruction.next))
        )

    def write_tape(self, tape):
        return "".join(map(self._write_alphabetic_char, tape.foreach()))

