class AbstractTape(object):
    """
    Turing machine tape interface
    """
    def move_right(self):
        raise NotImplementedError()

    def move_left(self):
        raise NotImplementedError()

    def move_down(self):
        raise NotImplementedError()

    def move_up(self):
        raise NotImplementedError()

    def read(self):
        """
        Returns the value at the current position
        """
        raise NotImplementedError()

    def write(self, value):
        """
        Writes the value at the current position
        """
        raise NotImplementedError()

    def render(self):
        """
        Prints a verbose representation of the tape contents
        """
        raise NotImplementedError()

    def short_render(self):
        """
        Prints a short representation of the tape contents
        """
        raise NotImplementedError()

    @classmethod
    def get_subclasses(cls, include_top=False):
        return ([cls] if include_top else []) + sum(
            (clz.get_subclasses(True)
            for clz in cls.__subclasses__()),
            []
        )


class Tape(AbstractTape):
    """
    Simple infinite tape
    """
    def __init__(self, data="", blank=None):
        self.tape = dict(enumerate(data))
        self.pos = 0
        self.blank = blank

    def move_right(self):
        self.pos += 1

    def move_left(self):
        self.pos -= 1

    def move_down(self):
        self.pos += 1

    def move_up(self):
        self.pos -= 1

    def read(self):
        return self.read_at(self.pos)

    def read_at(self, pos):
        return self.tape.get(pos, self.blank)

    def write(self, value):
        self.tape[self.pos] = value

    def written_range(self):
        if not self.tape:
            range_min = range_max = self.pos
        else:
            range_min = min(min(self.tape.keys()), self.pos)
            range_max = max(max(self.tape.keys()), self.pos)
        return xrange(range_min, range_max+1)

    def foreach(self, include_index=False):
        if include_index:
            for i in self.written_range():
                yield i, self.read_at(i)
        else:
            for i in self.written_range():
                yield self.read_at(i)

    def render(self):
        for i, val in self.foreach(True):
            txt = str(val) if val is not None else ''
            if i == 0:
                txt += " (start)"
            if i == self.pos:
                txt += " <--"
            print txt

    def short_render(self):
        print "".join(map(lambda x: str(x) if x is not None else ' ', self.foreach()))


class NumericTape(Tape):
    def __init__(self, data="", blank=None):
        return super(NumericTape, self).__init__(
            map(lambda x: int(x) if x != " " else blank, data),
            blank
        )


class TextTape(Tape):
    def __init__(self, data="", blank=" "):
        return super(TextTape, self).__init__(data, blank)


class Tape2D(AbstractTape):
    """
    Simple infinite 2D tape
    """
    def __init__(self, data=None, blank=None):
        self.tape = data if data is not None else {}
        self.pos_x = 0
        self.pos_y = 0
        self.blank = blank

    def move_right(self):
        self.pos_x += 1

    def move_left(self):
        self.pos_x -= 1

    def move_down(self):
        self.pos_y += 1

    def move_up(self):
        self.pos_y -= 1

    def read(self):
        return self.read_at(self.pos_x, self.pos_y)

    def read_at(self, x, y):
        return self.tape.get((x,y), self.blank)

    def write(self, value):
        self.tape[(self.pos_x, self.pos_y)] = value

    def written_range(self):
        if not self.tape:
            min_x = max_x = self.pos_x
            min_y = max_y = self.pos_y
        else:
            xs, ys = zip(*self.tape.keys())
            min_x = min(min(xs), self.pos_x)
            max_x = max(max(xs), self.pos_x)
            min_y = min(min(ys), self.pos_y)
            max_y = max(max(ys), self.pos_y)
        return xrange(min_x, max_x+1), xrange(min_y, max_y+1)

    def render(self):
        def _render_value(x, y):
            val = self.read_at(x, y) or ' '
            colors = []
            if x == 0 and y == 0:
                colors.append(4)
            if x == self.pos_x and y == self.pos_y:
                colors.append(7)
            if colors:
                return "\x1b[%sm%s\x1b[m" % (";".join(map(str, colors)), val)
            return val

        range_x, range_y = self.written_range()
        for y in range_y:
            print "".join(
                _render_value(x, y)
                for x in range_x
            )

    def short_render(self):
        range_x, range_y = self.written_range()
        for y in range_y:
            print "".join(
                self.read_at(x, y) or ' '
                for x in range_x
            )


class AntTape(Tape2D):
    """
    Tape for Langton's ant type of machine
    left/right turn the and and up/down move it
    """
    def __init__(self, *a, **k):
        super(AntTape, self).__init__(*a, **k)
        self.directions = [
            (1, 0),
            (0, 1),
            (-1, 0),
            (0, -1),
        ]
        self.direction_index = 0

    def move_right(self):
        self.direction_index += 1
        self.direction_index %= len(self.directions)
        self.move_up()

    def move_left(self):
        self.direction_index -= 1
        self.direction_index %= len(self.directions)
        self.move_up()

    def move_down(self):
        dx, dy = self.directions[self.direction_index]
        self.pos_x -= dx
        self.pos_y -= dy

    def move_up(self):
        dx, dy = self.directions[self.direction_index]
        self.pos_x += dx
        self.pos_y += dy


class Instruction(object):
    LEFT = staticmethod(lambda x: x.move_left())
    RIGHT = staticmethod(lambda x: x.move_right())
    UP = staticmethod(lambda x: x.move_up())
    DOWN = staticmethod(lambda x: x.move_down())
    NOOP = staticmethod(lambda x: None)

    def __init__(self, write, move, next):
        self.write = write
        self.move = move
        self.next = next


class MultiLinearTape(AbstractTape):
    """
    Tape handling multiple mono-dimensional tape,
    one for working memory (rw) and a few write-only one for output
    """
    def __init__(self, data="", input_type=Tape, output_type=None, blank=None,
                 output_move=Instruction.RIGHT, overwrite_input=True):
        self.input_tape = input_type(data, blank=blank)
        self.make_output_tape = lambda: (output_type or input_type)(blank=blank)
        self.outputs = {}
        self.output = 0
        self.overwrite_input = overwrite_input
        self.output_move = output_move
        self.output_active = False

    def start_output(self, output_move=None):
        """
        Enables writing to the output tape(s),
        can be used as an instruction move operation
        """
        self.output_active = True
        if output_move:
            self.output_move = output_move

    def stop_output(self):
        """
        Disables writing to the output tape(s),
        can be used as an instruction move operation
        """
        self.output_active = False

    def next_output(self):
        """
        Selects the next output tape,
        can be used as an instruction move operation
        """
        self.output += 1

    def previous_ouput(self):
        """
        Selects the previous output tape,
        can be used as an instruction move operation
        """
        self.output -= 1

    def start_overwrite_input(self):
        """
        Enables writing to the input tape,
        can be used as an instruction move operation
        """
        self.overwrite_input = True

    def stop_overwrite_input(self):
        """
        Disables writing to the input tape,
        can be used as an instruction move operation
        """
        self.overwrite_input = False

    def get_output_tape(self):
        if not self.output in self.outputs:
            self.outputs[self.output] = self.make_output_tape()
        return self.outputs[self.output]

    def move_right(self):
        self.input_tape.move_right()

    def move_left(self):
        self.input_tape.move_left()

    def move_down(self):
        self.input_tape.move_up()

    def move_up(self):
        self.input_tape.move_down()

    def read(self):
        return self.input_tape.read()

    def write(self, value):
        if self.overwrite_input:
            self.input_tape.write(value)

        if self.output_active:
            out = self.get_output_tape()
            self.output_move(out)
            out.write(value)

    def render(self):
        self.short_render()

    def short_render(self):
        self.input_tape.short_render()
        for tape_id, tape in sorted(self.outputs.items()):
            tape.short_render()

    def foreach(self, include_index=False):
        return self.input_tape.foreach(include_index)


class _Halt(object):
    def __init__(self):
        pass

    def __str__(self):
        return "Halt"

    def __unicode__(self):
        return unicode(str(self))


Halt = _Halt()


class Machine(object):
    def __init__(self, states, initial_state):
        self.states = states
        self.initial_state = initial_state


class MachineRunner(object):
    def __init__(self, machine, tape, debug=False):
        self.machine = machine
        self.tape = tape
        self.instruction = self.get_instruction(self.machine.initial_state)
        self.debug = debug

    def get_instruction(self, state_name):
        if state_name is Halt:
            return state_name
        return self.machine.states[(state_name, self.tape.read())]

    def execute_one(self):
        if self.halted():
            return

        if self.debug:
            print "---"
            self.tape.render()
            print "---"
            print self.instruction.next

        self.tape.write(self.instruction.write)
        self.instruction.move(self.tape)
        self.instruction = self.get_instruction(self.instruction.next)

    def execute(self, max_steps=-1):
        while not self.halted() and max_steps != 0:
            max_steps -= 1
            self.execute_one()
        return self.tape

    def halted(self):
        return self.instruction is Halt
