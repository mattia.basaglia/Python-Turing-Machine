from turing import *
from tm_parser import TMDescriptionWriter

# Tri-state busy beaver
"""
# "01,a,1>b,1>,b,0>c,1>b,c,1<c,1<a,"
MachineRunner(
    Machine(
        {
            ("a", 0): Instruction(1, Instruction.RIGHT, 'b'),
            ("a", 1): Instruction(1, Instruction.RIGHT, Halt),
            ("b", 0): Instruction(0, Instruction.RIGHT, 'c'),
            ("b", 1): Instruction(1, Instruction.RIGHT, 'b'),
            ("c", 0): Instruction(1, Instruction.LEFT,  'c'),
            ("c", 1): Instruction(1, Instruction.LEFT,  'a'),
        },
        "a"
    ),
    Tape(blank=0)
).execute().render()
"""

# Add 1 in binary
"""
# " 01,a, <b,0>a,1>a,b,1>,1>,0<b,"
MachineRunner(
    Machine(
        {
            ("a", None): Instruction(None, Instruction.LEFT, 'b'),
            ("a", 0): Instruction(0, Instruction.RIGHT, 'a'),
            ("a", 1): Instruction(1, Instruction.RIGHT, 'a'),

            ("b", None): Instruction(1, Instruction.RIGHT, Halt),
            ("b", 0): Instruction(1, Instruction.RIGHT, Halt),
            ("b", 1): Instruction(0, Instruction.LEFT, 'b'),
        },
        "a"
    ),
    Tape(map(int, "100011"))
).execute().render()
"""


# Add 1 in decimal
"""
" 0123456789,a, <b,0>a,1>a,2>a,3>a,4>a,5>a,6>a,7>a,8>a,9>a,b,1<,1<,2<,3<,4<,5<,6<,7<,8<,9<,0<b,"
MachineRunner(
    Machine(
        {
            ("a", None): Instruction(None, Instruction.LEFT, 'b'),
            ("a", 0): Instruction(0, Instruction.RIGHT, 'a'),
            ("a", 1): Instruction(1, Instruction.RIGHT, 'a'),
            ("a", 2): Instruction(2, Instruction.RIGHT, 'a'),
            ("a", 3): Instruction(3, Instruction.RIGHT, 'a'),
            ("a", 4): Instruction(4, Instruction.RIGHT, 'a'),
            ("a", 5): Instruction(5, Instruction.RIGHT, 'a'),
            ("a", 6): Instruction(6, Instruction.RIGHT, 'a'),
            ("a", 7): Instruction(7, Instruction.RIGHT, 'a'),
            ("a", 8): Instruction(8, Instruction.RIGHT, 'a'),
            ("a", 9): Instruction(9, Instruction.RIGHT, 'a'),

            ("b", None): Instruction(1, Instruction.LEFT, Halt),
            ("b", 0): Instruction(1, Instruction.LEFT, Halt),
            ("b", 1): Instruction(2, Instruction.LEFT, Halt),
            ("b", 2): Instruction(3, Instruction.LEFT, Halt),
            ("b", 3): Instruction(4, Instruction.LEFT, Halt),
            ("b", 4): Instruction(5, Instruction.LEFT, Halt),
            ("b", 5): Instruction(6, Instruction.LEFT, Halt),
            ("b", 6): Instruction(7, Instruction.LEFT, Halt),
            ("b", 7): Instruction(8, Instruction.LEFT, Halt),
            ("b", 8): Instruction(9, Instruction.LEFT, Halt),
            ("b", 9): Instruction(0, Instruction.LEFT, 'b'),
        },
        "a"
    ),
    Tape(map(int, "10012399"))
).execute().render()
"""


# Reverse Binary
"""
# " 01,a, <, >b, >c,d,1>e,0<d,1<d,c, <f,1>b,1>c,f, <d,0<f,1<f,b, <g,0>b,0>c,g, <h,0<g,1<g,e, >a,0>e,1>e,h,0>e,0<h,1<h,"
MachineRunner(
    Machine(
        {
            # Shift by 1 place to the righ (and delete last digit)
            ("Shift-s", None): Instruction(None, Instruction.LEFT, Halt),
            ("Shift-s", 0): Instruction(None, Instruction.RIGHT, 'Shift-0'),
            ("Shift-s", 1): Instruction(None, Instruction.RIGHT, 'Shift-1'),

            ("Shift-0", None): Instruction(None, Instruction.LEFT, 'Place-0'),
            ("Shift-0", 0): Instruction(0, Instruction.RIGHT, 'Shift-0'),
            ("Shift-0", 1): Instruction(0, Instruction.RIGHT, 'Shift-1'),

            ("Shift-1", None): Instruction(None, Instruction.LEFT, 'Place-1'),
            ("Shift-1", 0): Instruction(1, Instruction.RIGHT, 'Shift-0'),
            ("Shift-1", 1): Instruction(1, Instruction.RIGHT, 'Shift-1'),

            # Place a digit before the first blank
            ("Place-0", None): Instruction(None, Instruction.LEFT, 'Place-0-done'),
            ("Place-0", 0): Instruction(0, Instruction.LEFT, 'Place-0'),
            ("Place-0", 1): Instruction(1, Instruction.LEFT, 'Place-0'),

            ("Place-0-done", None): Instruction(0, Instruction.RIGHT, 'Rewind'),
            ("Place-0-done", 0): Instruction(0, Instruction.LEFT, 'Place-0-done'),
            ("Place-0-done", 1): Instruction(1, Instruction.LEFT, 'Place-0-done'),

            ("Place-1", None): Instruction(None, Instruction.LEFT, 'Place-1-done'),
            ("Place-1", 0): Instruction(0, Instruction.LEFT, 'Place-1'),
            ("Place-1", 1): Instruction(1, Instruction.LEFT, 'Place-1'),

            ("Place-1-done", None): Instruction(1, Instruction.RIGHT, 'Rewind'),
            ("Place-1-done", 0): Instruction(0, Instruction.LEFT, 'Place-1-done'),
            ("Place-1-done", 1): Instruction(1, Instruction.LEFT, 'Place-1-done'),

            # Find start (the position after the first blank)
            ("Rewind", None): Instruction(None, Instruction.RIGHT, "Shift-s"),
            ("Rewind", 0): Instruction(0, Instruction.RIGHT, 'Rewind'),
            ("Rewind", 1): Instruction(1, Instruction.RIGHT, 'Rewind'),
        },
        "Shift-s"
    ),
    Tape(map(int, "10001101"))
).execute().render()
"""


# Strlen, writes the number of consecutive non-blank places in the tapes (in binary)
"""
# " 01,a,0>, <b, <b,b,1>c,1>,1>,d, <e,0>d,1>d,f, <g,0<f,1<f,g,1>c,1>c,0<g,c, >d,0>c,1>c,e, <, <f, <f,"
MachineRunner(
    Machine(
        {
            # Initialize the counter (1 blank to the left of the string)
            ("Start", None): Instruction(0, Instruction.RIGHT, Halt),           # Empty tape, just write 0 and halt
            ("Start", 0): Instruction(None, Instruction.LEFT, "Start-write"),   # If not empty, write a blank
            ("Start", 1): Instruction(None, Instruction.LEFT, "Start-write"),   # and initialize the counter with 1

            ("Start-write", None): Instruction(1, Instruction.RIGHT, "GotoEnd"),# Write the 1 (the tape isn't empty)
            ("Start-write", 0): Instruction(1, Instruction.RIGHT, Halt),        # The tape has garbage data
            ("Start-write", 1): Instruction(1, Instruction.RIGHT, Halt),        # The tape has garbage data

            # Go to the end of the string
            ("GotoEnd", None): Instruction(None, Instruction.RIGHT, "FindEnd"), # Skip the counter digits
            ("GotoEnd", 0): Instruction(0, Instruction.RIGHT, "GotoEnd"),
            ("GotoEnd", 1): Instruction(1, Instruction.RIGHT, "GotoEnd"),

            ("FindEnd", None): Instruction(None, Instruction.LEFT, "Found"),    # Skip until a blank
            ("FindEnd", 0): Instruction(0, Instruction.RIGHT, "FindEnd"),
            ("FindEnd", 1): Instruction(1, Instruction.RIGHT, "FindEnd"),

            # Record the fact we found a character
            ("Found", None): Instruction(None, Instruction.LEFT, Halt),         # Nothing left
            ("Found", 0): Instruction(None, Instruction.LEFT, "FindCounter"),   # Remove the last digit
            ("Found", 1): Instruction(None, Instruction.LEFT, "FindCounter"),

            # Go back to the counter
            ("FindCounter", None): Instruction(None, Instruction.LEFT, "Increment"),
            ("FindCounter", 0): Instruction(0, Instruction.LEFT, "FindCounter"),
            ("FindCounter", 1): Instruction(1, Instruction.LEFT, "FindCounter"),

            # Add 1 to the counter
            ("Increment", None): Instruction(1, Instruction.RIGHT, "GotoEnd"),
            ("Increment", 0): Instruction(1, Instruction.RIGHT, "GotoEnd"),
            ("Increment", 1): Instruction(0, Instruction.LEFT, "Increment"),
        },
        "Start"
    ),
    Tape(map(int, "100011101")),
).execute().render()
"""

# Decrement binary integer
"""
# " 01,101100,a, <b,0>a,1>a,b,1>,1<b,0>,"
MachineRunner(
    Machine(
        {
            ("find-right", None): Instruction(None, Instruction.LEFT, 'decrement'),
            ("find-right", 0): Instruction(0, Instruction.RIGHT, 'find-right'),
            ("find-right", 1): Instruction(1, Instruction.RIGHT, 'find-right'),

            ("decrement", None): Instruction(1, Instruction.RIGHT, Halt),
            ("decrement", 0): Instruction(1, Instruction.LEFT, 'decrement'),
            ("decrement", 1): Instruction(0, Instruction.RIGHT, Halt),
        },
        "find-right"
    ),
    Tape(map(int, "101100")),
).execute().render()
"""

# Binary Sum
"""
# " 01,a, >b,0>a,1>a,b, <c,0>b,1>b,d, <e,0<d,1<d,f, <, >f, >f,e,1-a,1-a,0<e,c, >f,1<c,0-d,"
MachineRunner(
    Machine(
        {
            # Go to the end (needs to skip a blank)
            ("find-end", None): Instruction(None, Instruction.RIGHT, "find-end-n2"),
            ("find-end", 0): Instruction(0, Instruction.RIGHT, "find-end"),
            ("find-end", 1): Instruction(1, Instruction.RIGHT, "find-end"),

            ("find-end-n2", None): Instruction(None, Instruction.LEFT, "decrement"),
            ("find-end-n2", 0): Instruction(0, Instruction.RIGHT, "find-end-n2"),
            ("find-end-n2", 1): Instruction(1, Instruction.RIGHT, "find-end-n2"),

            # Decrement n2
            ("decrement", None): Instruction(None, Instruction.RIGHT, "clear"),
            ("decrement", 0): Instruction(1, Instruction.LEFT, "decrement"),
            ("decrement", 1): Instruction(0, Instruction.NOOP, "find-n1"),

            # Find the end of n1
            ("find-n1", None): Instruction(None, Instruction.LEFT, "increment"),
            ("find-n1", 0): Instruction(0, Instruction.LEFT, "find-n1"),
            ("find-n1", 1): Instruction(1, Instruction.LEFT, "find-n1"),

            # Increment n1
            ("increment", None): Instruction(1, Instruction.NOOP, "find-end"),
            ("increment", 0): Instruction(1, Instruction.NOOP, "find-end"),
            ("increment", 1): Instruction(0, Instruction.LEFT, "increment"),

            # Clear n2
            ("clear", None): Instruction(None, Instruction.LEFT, Halt),
            ("clear", 0): Instruction(None, Instruction.RIGHT, "clear"),
            ("clear", 1): Instruction(None, Instruction.RIGHT, "clear"),

        },
        "find-end"
    ),
    NumericTape("10011 110"),
).execute().render()
"""

# Square
"""
MachineRunner(
    Machine(
        {
            ("drawr1", "  "): Instruction("[]", Instruction.RIGHT, "drawr2"),
            ("drawr2", "  "): Instruction("[]", Instruction.RIGHT, "drawr3"),
            ("drawr3", "  "): Instruction("[]", Instruction.RIGHT, "drawd1"),
            ("drawd1", "  "): Instruction("[]", Instruction.DOWN, "drawd2"),
            ("drawd2", "  "): Instruction("[]", Instruction.DOWN, "drawd3"),
            ("drawd3", "  "): Instruction("[]", Instruction.DOWN, "drawl1"),
            ("drawl1", "  "): Instruction("[]", Instruction.LEFT, "drawl2"),
            ("drawl2", "  "): Instruction("[]", Instruction.LEFT, "drawl3"),
            ("drawl3", "  "): Instruction("[]", Instruction.LEFT, "drawu1"),
            ("drawu1", "  "): Instruction("[]", Instruction.UP, "drawu2"),
            ("drawu2", "  "): Instruction("[]", Instruction.UP, "drawu3"),
            ("drawu3", "  "): Instruction("[]", Instruction.UP, Halt),

        },
        "drawr1"
    ),
    Tape2D(blank="  "),
).execute().render()
"""

# Ant Square
"""
MachineRunner(
    Machine(
        {
            ("draw1", " "): Instruction("x", Instruction.UP, "draw2"),
            ("draw1", "x"): Instruction("x", Instruction.NOOP, Halt),
            ("draw2", " "): Instruction("x", Instruction.UP, "turn"),
            ("turn", " "): Instruction("x", Instruction.RIGHT, "draw1"),
        },
        "draw1"
    ),
    AntTape(blank=" "),
).execute().render()
"""

# Langton's ant (Note: doesn't halt)
"""
MachineRunner(
    Machine(
        {
            ("ant", " "): Instruction("x", Instruction.RIGHT, "ant"),
            ("ant", "x"): Instruction(" ", Instruction.LEFT, "ant"),
        },
        "ant"
    ),
    AntTape(blank=" "),
).execute(200).render()
"""

# Copy
"""
MachineRunner(
    Machine(
        {
            ("copy", None): Instruction(None, Instruction.RIGHT, Halt),
            ("copy", 0): Instruction(0, Instruction.RIGHT, "copy"),
            ("copy", 1): Instruction(1, Instruction.RIGHT, "copy"),
        },
        "copy"
    ),
    MultiLinearTape("100110", NumericTape),
).execute().render()
"""


# Decrement binary integer to an output tape, keeping the input tape unchanged
"""
MachineRunner(
    Machine(
        {
            ("find-right", None): Instruction(None, Instruction.NOOP, 'change-tape'),
            ("find-right", 0): Instruction(0, Instruction.RIGHT, 'find-right'),
            ("find-right", 1): Instruction(1, Instruction.RIGHT, 'find-right'),

            ("change-tape", None): Instruction(None, MultiLinearTape.start_output, 'start-decrement'),

            ("start-decrement", None): Instruction(None, Instruction.LEFT, 'decrement'),

            ("decrement", None): Instruction(1, Instruction.LEFT, Halt),
            ("decrement", 0): Instruction(1, Instruction.LEFT, 'decrement'),
            ("decrement", 1): Instruction(0, Instruction.LEFT, "finish"),

            ("finish", None): Instruction(None, Instruction.LEFT, Halt),
            ("finish", 0): Instruction(0, Instruction.LEFT, 'finish'),
            ("finish", 1): Instruction(1, Instruction.LEFT, 'finish'),
        },
        "find-right"
    ),
    MultiLinearTape("1001100", NumericTape, output_move=Instruction.LEFT, overwrite_input=False),
).execute().render()
"""


# Replicate the input pattern multiple times on an output tape
"""
MachineRunner(
    Machine(
        {
            ("replicate", " "): Instruction(" ", MultiLinearTape.stop_output, "reset"),
            ("replicate", "0"): Instruction("0", Instruction.RIGHT, "replicate"),
            ("replicate", "1"): Instruction("1", Instruction.RIGHT, "replicate"),

            ("reset", " "): Instruction(" ", Instruction.LEFT, "go-back"),

            ("go-back", " "): Instruction(" ", Instruction.RIGHT, "start-replicate"),
            ("go-back", "0"): Instruction("0", Instruction.LEFT, "go-back"),
            ("go-back", "1"): Instruction("1", Instruction.LEFT, "go-back"),

            ("start-replicate", " "): Instruction(" ", MultiLinearTape.start_output, "replicate"),
            ("start-replicate", "0"): Instruction("0", MultiLinearTape.start_output, "replicate"),
            ("start-replicate", "1"): Instruction("1", MultiLinearTape.start_output, "replicate"),

        },
        "replicate"
    ),
    MultiLinearTape("10011", blank=" ", overwrite_input=False),
).execute(103).render()
"""
