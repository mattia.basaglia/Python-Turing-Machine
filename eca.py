def _to_int(n, digits):
    return ("{0:0%sb}" % digits).format(n)


class Rule(object):
    def __init__(self, rule):
        if isinstance(rule, str):
            outputs = rule.rjust(8, '0')
            rule = int(rule, 2)
        else:
            outputs = _to_int(rule, 8)

        self.rule = rule

        if len(outputs) != 8:
            raise ValueError("Invalid rule number: %s" % rule)

        self.rules = {
            tuple(_to_int(index, 3)): output
            for index, output in enumerate(reversed(outputs))
        }

    def _get_value(self, input, index):
        return input[index % len(input)]

    def _get_output(self, input, index):
        left = self._get_value(input, index-1)
        middle = self._get_value(input, index)
        right = self._get_value(input, index+1)
        return self.rules[left, middle, right]

    def next(self, input):
        if not input:
            return input
        return "".join(self._get_output(input, index)
                       for index in xrange(len(input)))

    def __str__(self):
        return "Rule %s" % self.rule


class ElementaryCellurarAutomation(object):
    def __init__(self, rule, input):
        if isinstance(rule, Rule):
            self.rule = rule
        else:
            self.rule = Rule(rule)

        self.input = input

    def __iter__(self):
        return self

    def __next__(self):
        current = self.input
        self.input = self.rule.next(self.input)
        return current

    def next(self):
        return self.__next__()


class BoundElementaryCellurarAutomation(ElementaryCellurarAutomation):
    def __init__(self, rule, input, limit):
        super(BoundElementaryCellurarAutomation, self).__init__(rule, input)
        self.limit = limit

    def __iter__(self):
        input = self.input
        for i in xrange(self.limit):
            yield input
            input = self.rule.next(input)
