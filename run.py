#!/usr/bin/env python

import sys
from argparse import ArgumentParser

from tm_parser import TMDescriptionParser
import turing


argument_parser = ArgumentParser(description="Turing machine parser/runner")
argument_parser.add_argument(
    "--inline-tape",
    "-t",
    choices=["auto", "force", "none"],
    default="auto",
    help="Whether to parse the tape contents from the machine description or from a second argument",
)

argument_parser.add_argument(
    "--max-steps",
    "-s",
    type=int,
    default=-1,
    help="Maximum number of steps to execute",
)

argument_parser.add_argument(
    "--debug",
    "-d",
    action="store_true",
    default=False,
    help="Debug machine execution",
)

argument_parser.add_argument(
    "--output-format",
    "-f",
    choices=["none", "short", "long"],
    default="short",
    help="Ouput tape formatter",
)

argument_parser.add_argument(
    "--initial-state",
    "-i",
    default=None,
    help="Name of the initial state (by default the first defined one)",
)

argument_parser.add_argument(
    "--tape-type",
    "-T",
    default="Tape",
    choices=[t.__name__ for t in turing.AbstractTape.get_subclasses()],
    help="Name of the type of the tape",
)


argument_parser.add_argument(
    "--2d",
    "-2",
    nargs="?",
    default="",
    choices=["ant"],
    help="Shorthand to switch to 2D mode (-t none -T Tape2D or AntTape)",
    dest="twod",
)


argument_parser.add_argument(
    "machine",
)

argument_parser.add_argument(
    "tape",
    nargs='?',
)

arguments = argument_parser.parse_args()

if arguments.twod != "":
    parse_tape = False
    tape_type = turing.Tape2D if arguments.twod != "ant" else turing.AntTape
else:
    if arguments.inline_tape == "auto":
        parse_tape = arguments.tape is None
    else:
        parse_tape = arguments.inline_tape == "force"

    tape_type = getattr(turing, arguments.tape_type)

runner_args = TMDescriptionParser.machine_from_string(arguments.machine, parse_tape, tape_type)
if not parse_tape:
    tape_kwargs = {
        "blank": runner_args.blank,
    }
    if arguments.tape:
        tape_kwargs["data"] = arguments.tape
    runner_args = (runner_args, tape_type(**tape_kwargs))

if arguments.initial_state:
    runner_args[0].initial_state = arguments.initial_state
tape = turing.MachineRunner(*runner_args, debug=arguments.debug).execute(arguments.max_steps)

if arguments.output_format == "long":
    tape.render()
elif arguments.output_format == "short":
    tape.short_render()
